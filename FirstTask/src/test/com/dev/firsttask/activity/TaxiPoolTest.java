package test.com.dev.firsttask.activity;

import com.dev.firsttask.action.CountCost;
import com.dev.firsttask.action.SearchVehicle;
import com.dev.firsttask.action.SortTaxiPool;
import com.dev.firsttask.creation.CreatingTaxiPool;
import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.exception.CountCostException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TaxiPoolTest {

    private static TaxiPool taxiPool;

    @BeforeClass
    public static void initTaxiPool() {
        taxiPool = new TaxiPool(CreatingTaxiPool.createTaxiPool());
    }

    @AfterClass
    public static void clearTaxiPool() {
        taxiPool = null;
    }

    @Test
    public void countCostPoolOneTest() throws CountCostException {
        final double INFLATION = 1.2;
        int expected = 0;
        for (Taxi veh : taxiPool.getPool()) {
            expected += veh.getCost();
        }
        expected *= INFLATION;
        int actual = CountCost.countCostPool(taxiPool, INFLATION);
        Assert.assertEquals(expected, actual, 0.01);
    }

    @Test(expected = CountCostException.class)
    public void countCostPoolTwoTest() throws CountCostException {
        final double INFLATION = -1.2;
        int expected = 0;
        for (Taxi veh : taxiPool.getPool()) {
            expected += veh.getCost();
        }
        expected *= INFLATION;
        int actual = CountCost.countCostPool(taxiPool, INFLATION);
        Assert.assertEquals(expected, actual, 0.01);
    }

    @Test
    public void searchByCapAndCarryAbTest() {
        final int MIN_CAPACITY = 3;
        final int MAX_CAPACITY = 12;
        final double MIN_CARRY = 700;
        final double MAX_CARRY = 3100;
        List<Taxi> actual = SearchVehicle.searchByCapAndCarryAb(taxiPool, MIN_CAPACITY, MAX_CAPACITY, MIN_CARRY, MAX_CARRY);
        ArrayList<Taxi> expected = new ArrayList<Taxi>();
        List<Taxi> resultByCarryingAbility = SearchVehicle.searchByCarryingAbility(taxiPool, MIN_CARRY, MAX_CARRY);
        for (Taxi vehicle : resultByCarryingAbility) {
            if (vehicle.getCapacity() >= MIN_CAPACITY && vehicle.getCapacity() <= MAX_CAPACITY) {
                expected.add(vehicle);
            }
        }
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sortPoolTest() {
        SortTaxiPool.sortPool(taxiPool);
        List<Taxi> pool = taxiPool.getPool();
        boolean isOk = true;
        for (int i = 0; i < pool.size() - 1; i++) {
            if (pool.get(i).getConsumptionFuel() > pool.get(i + 1).getConsumptionFuel()) {
                isOk = false;
                break;
            }
        }
        Assert.assertTrue(isOk);
    }
}
