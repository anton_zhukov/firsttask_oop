package com.dev.firsttask.entity;

public class Vehicle implements Comparable<Vehicle> {

    private int cost;
    private double consumptionFuel;
    private double carryingAbility;
    private int capacity;
    private String registrationNumber;

    public Vehicle(int cost, double consumptionFuel, double carryingAbility, int capacity, String registerNumber) {
        this.cost = cost;
        this.consumptionFuel = consumptionFuel;
        this.carryingAbility = carryingAbility;
        this.capacity = capacity;
        this.registrationNumber = registerNumber;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        if (cost > 0) {
            this.cost = cost;
        } else {
            throw new IllegalArgumentException("Cost can't be negative");
        }
    }

    public double getConsumptionFuel() {
        return consumptionFuel;
    }

    public void setConsumptionFuel(double consumptionFuel) {
        if (consumptionFuel > 0) {
            this.consumptionFuel = consumptionFuel;
        } else {
            throw new IllegalArgumentException("ConsumptionFuel can't be negative");
        }
    }

    public double getCarryingAbility() {
        return carryingAbility;
    }

    public void setCarryingAbility(double carryingAbility) {
        if (carryingAbility > 0) {
            this.carryingAbility = carryingAbility;
        } else {
            throw new IllegalArgumentException("CarryingAbility can't be negative");
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        if (capacity > 0) {
            this.capacity = capacity;
        } else {
            throw new IllegalArgumentException("Capacity can't be negative");
        }
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    @Override
    public String toString() {
        return "Vehicle" +
                "\nregistrationNumber" + registrationNumber +
                "\ncost=" + cost +
                "\nconsumptionFuel=" + consumptionFuel +
                "\ncarryingAbility=" + carryingAbility +
                "\ncapacity=" + capacity + "\n";
    }

    @Override
    public int compareTo(Vehicle o1) {
        return Double.compare(this.getConsumptionFuel(), o1.getConsumptionFuel());
    }
}
