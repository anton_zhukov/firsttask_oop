package com.dev.firsttask.entity;

public class TaxiLorry extends Taxi {

    private double capacityCargoBay;

    public TaxiLorry(int cost, double consumptionFuel, double carryingAbility, int capacity,
                     String registerNumber, TaxiService taxiServiceName, double capacityCargoBay) {
        super(cost, consumptionFuel, carryingAbility, capacity, registerNumber, taxiServiceName);
        this.capacityCargoBay = capacityCargoBay;
    }

    public double getCapacityCargoBay() {
        return capacityCargoBay;
    }

    public void setCapacityCargoBay(double capacityCargoBay) {
        if (capacityCargoBay > 0) {
            this.capacityCargoBay = capacityCargoBay;
        } else {
            throw new IllegalArgumentException("CapacityCargoBay can't be negative");
        }
    }

    @Override
    public String toString() {
        return "TaxiLorry" +
                "\nregistrationNumber" + super.getRegistrationNumber() +
                "\ncapacityCargoBay=" + capacityCargoBay +
                "\nTaxi Service = {" + super.getTaxiServiceName() + "}" +
                "\ncost=" + super.getCost() +
                "\nconsumptionFuel=" + super.getConsumptionFuel() +
                "\ncarryingAbility=" + super.getCarryingAbility() +
                "\ncapacity=" + super.getCapacity() + "\n";
    }
}
