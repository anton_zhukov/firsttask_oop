package com.dev.firsttask.entity;


public class Taxi extends Vehicle {

    private TaxiService taxiServiceName;

    public Taxi(int cost, double consumptionFuel, double carryingAbility,
                int capacity, String registerNumber, TaxiService taxiServiceName) {
        super(cost, consumptionFuel, carryingAbility, capacity, registerNumber);
        this.taxiServiceName = taxiServiceName;
    }

    public TaxiService getTaxiServiceName() {
        return taxiServiceName;
    }

    public void setTaxiServiceName(TaxiService taxiServiceName) {
        this.taxiServiceName = taxiServiceName;
    }

    @Override
    public String toString() {
        return "Taxi" +
                "\nregistrationNumber" + super.getRegistrationNumber() +
                "\nTaxi Service = {" + getTaxiServiceName() + "}" +
                "\ncost=" + super.getCost() +
                "\nconsumptionFuel=" + super.getConsumptionFuel() +
                "\ncarryingAbility=" + super.getCarryingAbility() +
                "\ncapacity=" + super.getCapacity() + "\n";
    }
}