package com.dev.firsttask.entity;

import java.util.Comparator;


public class RegistrationNumberComparator implements Comparator<Vehicle> {
    @Override
    public int compare(Vehicle obj1, Vehicle obj2) {
        if (obj1.getRegistrationNumber() == obj2.getRegistrationNumber()) {
            return 0;
        }
        if (obj1.getRegistrationNumber() == null) {
            return -1;
        }
        if (obj2.getRegistrationNumber() == null) {
            return 1;
        }
        return obj1.getRegistrationNumber().compareTo(obj2.getRegistrationNumber());
    }
}

