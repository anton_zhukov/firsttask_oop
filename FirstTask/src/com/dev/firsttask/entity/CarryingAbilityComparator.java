package com.dev.firsttask.entity;

import java.util.Comparator;

public class CarryingAbilityComparator implements Comparator<Vehicle> {
    @Override
    public int compare(Vehicle o1, Vehicle o2) {
        return Double.compare(o1.getCarryingAbility(), o2.getCarryingAbility());
    }
}
