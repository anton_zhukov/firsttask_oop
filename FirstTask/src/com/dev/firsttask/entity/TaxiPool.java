package com.dev.firsttask.entity;

import java.util.List;

public class TaxiPool {

    private List<Taxi> pool;

    public TaxiPool(List<Taxi> pool) {
        this.pool = pool;
    }

    public List<Taxi> getPool() {
        return pool;
    }
}
