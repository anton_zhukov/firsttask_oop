package com.dev.firsttask.action;

import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.exception.CountCostException;
import org.apache.log4j.Logger;

public class CountCost {

    private static final Logger LOG = Logger.getLogger(CountCost.class);

    /**
     * @param pool         the reference object for getting list of Taxi instances
     * @param coefInlation Coefficient of inflation
     * @return if parameters are correct, we will gain cost of taxi-pool adjusted for inflation
     * @throws CountCostException if parameter {@code inflation} is not correct, that kind of exception will be thrown
     */
    public static int countCostPool(TaxiPool pool, double coefInlation) throws CountCostException {
        int result = 0;

        if (coefInlation < 1) {
            throw new CountCostException("Coefficient of inflation = " + coefInlation + " is less than 1.0");
        }

        LOG.info("Argument coefInflation is " + coefInlation);
        for (Taxi veh : pool.getPool()) {
            result += veh.getCost();
        }
        return (int) (result * coefInlation);
    }
}