package com.dev.firsttask.action;

import com.dev.firsttask.entity.CarryingAbilityComparator;
import com.dev.firsttask.entity.RegistrationNumberComparator;
import com.dev.firsttask.entity.TaxiPool;

import java.util.Collections;

public class SortTaxiPool {

    /**
     * Method for sorting by Fuel consumption
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortPool(TaxiPool pool) {
        Collections.sort(pool.getPool());
    }

    /**
     * Method for sorting by Carrying Ability
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortByCarryingAbility(TaxiPool pool) {
        Collections.sort(pool.getPool(), new CarryingAbilityComparator());
    }

    /**
     * Method for sorting by Registration Number
     *
     * @param pool the reference object for getting list of {@code Taxi} instances
     */
    public static void sortByRegistrationNumber(TaxiPool pool) {
        Collections.sort(pool.getPool(), new RegistrationNumberComparator());
    }
}
