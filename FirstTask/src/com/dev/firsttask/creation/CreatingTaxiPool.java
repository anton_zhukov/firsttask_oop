package com.dev.firsttask.creation;

import com.dev.firsttask.entity.MiniBus;
import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiLorry;
import com.dev.firsttask.entity.TaxiService;

import java.util.ArrayList;
import java.util.List;

public class CreatingTaxiPool {

    /**
     * Method for creating Taxi-Pool
     *
     * @return Back created list of Taxi-Pool's vehicles
     */
    public static List<Taxi> createTaxiPool() {
        List<Taxi> pool = new ArrayList<Taxi>();
        pool.add(new Taxi(14900, 10.2, 600, 5, "1654 BC-7", TaxiService.Chrystal_7778));
        pool.add(new TaxiLorry(27000, 18.1, 3500, 3, "KM 1234-7", TaxiService.TaxiCargo_163, 18.354));
        pool.add(new MiniBus(21550, 16.8, 1500, 12, "2657 BC-7", TaxiService.ALMAZ_7788, "00165498"));
        pool.add(new Taxi(22500, 16.2, 740, 5, "7658 OE-7", TaxiService.VIP_107));
        pool.add(new MiniBus(22570, 15.9, 1540, 14, "4653 IC-7", TaxiService.Stolitsa_135, "02165942"));
        return pool;
    }
}