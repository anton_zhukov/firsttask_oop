package com.dev.firsttask.main;

import com.dev.firsttask.action.CountCost;
import com.dev.firsttask.action.SearchVehicle;
import com.dev.firsttask.action.SortTaxiPool;
import com.dev.firsttask.creation.CreatingTaxiPool;
import com.dev.firsttask.entity.Taxi;
import com.dev.firsttask.entity.TaxiPool;
import com.dev.firsttask.exception.CountCostException;
import org.apache.log4j.Logger;

import java.util.List;

public class Runner {

    private static final Logger LOG = Logger.getLogger(Runner.class);

    public static void main(String[] args) {

        // Creating TaxiPool
        TaxiPool taxiPool = new TaxiPool(CreatingTaxiPool.createTaxiPool());
        LOG.info("Taxi-pool is created");

        //Sorting TaxiPool by Consumption of Fuel
        SortTaxiPool.sortPool(taxiPool);
        LOG.info("Taxi-pool is sorted by Consumption of Fuel:");

        for (Taxi vehicle : taxiPool.getPool()) {
            LOG.debug("Fuel: " + vehicle.getConsumptionFuel() + "L per 100km"
                    + ", Reg. number: " + vehicle.getRegistrationNumber()
                    + ", Taxi Service: " + vehicle.getTaxiServiceName()
                    + ", Carrying ability: " + vehicle.getCarryingAbility() + " kg");
        }

        //Sorting TaxiPool by Carrying Ability
        SortTaxiPool.sortByCarryingAbility(taxiPool);
        LOG.info("Taxi-pool is sorted by Carrying Ability:");

        for (Taxi vehicle : taxiPool.getPool()) {
            LOG.debug("Carrying ability: " + vehicle.getCarryingAbility()
                    + " kg, Reg. number: " + vehicle.getRegistrationNumber()
                    + ", Taxi Service: " + vehicle.getTaxiServiceName()
                    + ", Fuel: " + vehicle.getConsumptionFuel() + "L per 100km");
        }

        //Sorting TaxiPool by Registration Number
        SortTaxiPool.sortByRegistrationNumber(taxiPool);
        LOG.info("Taxi-pool is sorted by Registration Number:");

        for (Taxi vehicle : taxiPool.getPool()) {
            LOG.debug("Reg. number: " + vehicle.getRegistrationNumber()
                    + ", Taxi Service: " + vehicle.getTaxiServiceName()
                    + ", Carrying ability: " + vehicle.getCarryingAbility()
                    + " kg, Fuel: " + vehicle.getConsumptionFuel() + "L per 100km");
        }

        // Counting cost of Taxi-Pool
        final double INFLATION = 1.2;
        try {
            LOG.info("Taxi-pool costs " + CountCost.countCostPool(taxiPool, INFLATION) + "$ adjusted for inflation");
        } catch (CountCostException e) {
            LOG.error("Wrong value of inflation coefficient");
        }

        // Ranges for searching
        final int MIN_CAPACITY = 3;
        final int MAX_CAPACITY = 12;
        final double MIN_CARRY = 700;
        final double MAX_CARRY = 3100;

        // Searching Vehicles by Capacity And CarryingAbility
        List<Taxi> result = SearchVehicle.searchByCapAndCarryAb(taxiPool, MIN_CAPACITY, MAX_CAPACITY, MIN_CARRY, MAX_CARRY);
        LOG.info("Searching by Capacity And Carrying Ability is done.");
        for (Taxi vehicle : result)
            LOG.debug("Reg. number: " + vehicle.getRegistrationNumber() +
                    ", Taxi Service: " + vehicle.getTaxiServiceName() +
                    ", Capacity: " + vehicle.getCapacity() + " persons, Carrying ability: " +
                    vehicle.getCarryingAbility() + " kg");
    }
}
